import re

from pip._vendor.distlib.compat import raw_input

PASSWORD_REGEX = "^(?=.*[0-9])(?=.*[a-z])" \
                 "(?=.*[A-Z])(?=.*[@#$%^&+=]" \
                 ")(?=\\S+$).{6,12}$"

password = raw_input('Enter a password\n')

if re.findall(PASSWORD_REGEX, password):
    print('Valid password')
else:
    print('Invalid password')












# Write a program which will find all such numbers
# which are divisible by 7 but are not a multiple of 4.
# between 1 and 100 (both included).
# The numbers obtained should be printed in a comma-separated
# sequence on a single line.
nums = []
for i in range(1, 101):
    if (i % 7 == 0) and (i % 4 != 0):
        nums.append(str(i))

print(','.join(nums))

"""
Output : -
7,14,21,35,42,49,63,70,77,91,98
"""


array = [4, 2, 7, 5, 12, 54, 21, 64, 12, 32]
print('List has the items: ', array)

searchItem = int(input('Enter a number to search for: '))

found = False

for i in range(len(array)):
    if array[i] == searchItem:
        found = True
        print(searchItem, ' was found in the array at index ', i)
        break

if not found:
    print(searchItem, ' was not found in the array!')

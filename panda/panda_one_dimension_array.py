import pandas as pd

# Creating one dimensional array
array = pd.Series([2, 4, 6, 8, 5, 2])

# and it will print it with index numbers
print(array)

# print the type of an array
print(type(array))

# Convert pandas to python list
print(array.tolist())

# printing type of the list
print(type(array.tolist()))

secondArray = pd.Series([1, 5, 9, 7, 8])

# element to element addition will perform
# Example: array[1] + secondArray[1] = mergedArray[1]
mergedArray = array + secondArray
print(mergedArray)
